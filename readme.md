# Development

All code is in HTML/CSS/JS(web-components), with low dependencies (and
CDN imports), an no build/tooling (though you should use a `.editorconfig`
plugin in your editor).

## Local server
Run dev server with:

```
serve -s . // would need `npm i -g serve`
// or
npx serve -s .
```

> 

> The `-s` parameter is for the local server to behave as a **s**ingle
> page server; that way the URLs will be handled by the javascript
> client (served at `/`), and not by the server (on first
> load). Otherwise, the client would not catch the app's sub routes.

## rtc components

We're using a bunch of rtc components to connect peers with webrtc,
without the need of servers.

There are different scenarios we're trying to cover.

1. 1 peer alone -> no data channel
1. 2 peers, on the same browser, in 1 page
1. 2 peers, on the same browser, in 1 page, in 2 iframes
1. 2 peers, on the same browser, in 2 pages (tabs which are publich; not private browsing)
1. 2 peers, on the same browser, 1 public page, and 1 in private browsing page
1. 2 peers, on different browsers (private or public pages), on the same computer
1. 2 peers, on different browsers (private or public pages), on different computers, on the same network
1. 2 peers, on different browsers (private or public tabs), on different computers, on different networks (behind NAT/ROUTER)
1. 2 peers, different computer/browser/network, through private VPN
1. X peers, <- start again from 1.

It would be nice to have most of them working without the need of any server (signaling/TURN; STUN, since free are okay)?

Status:
- done: 1, 2, 3, 4
- doing: 5, 6, 7, (8 & 9)
- after: 10

## openpgp

Docs: https://openpgpjs.org/

In `./src/crypt` is exporte `openpgp`, so it is possible to encrypt and decrypt data.

## remote-storage

Docs: https://remotestorage.io

In `./src/storage/` are exported `NotesStorage, RsWidget,
RsWidgetStatus` to store data using `remote-storage`, in a "user only controled" storage place.

## router

Docs: https://vaadin.com/router

In `./src/router/` is exported `Router`, to build router components.

The `app` router, is where the main pages of the project are defined.

All pages components are themselves stored in `./src/pages/`.

# Notes

- user needsNO server to play on/* (and most, needs no internet
  connectivity) (+ later absolute 0 deps)
- user learn how to createremote-storage (no-google/dropbox because we
  need to create apps there to have api keys, and that is annoying,
  but the infra already supports it, just hidden) <- RS is not needed
  for most of the game, though it also handles local storage with no
  connectivity, through tabs, and browser refresh

Wwith the remote-storage, a user will store their data like so:

- `<remote-storage-host>/private/<app-name>/{myGpgKey,myFriendGpgKey,gameData}`
- `<remote-storage-host>/public/<remote-storage-user>/<app-name>/{public-data}`

That way, a user sharing their username in public/semi-private(share),
can make some data accessible (which a link of, can be stored in your
private RS)

Also, at first, you connect to a peer using Web Socket like with
manual peer exchange. When we're good to go, and peer can be
associated with their public GPG key, that we retrieve from their RS,
it will be easier to identify who's who from the new peer connection
an new peer connection with GPG.


# Licence

(a)GPLv3 https://www.gnu.org/licenses/gpl-3.0.en.html
