class LogInfo extends HTMLElement {
	log = (message) => {
		const $log = document.createElement('pre')
		$log.innerText = message

		this.prepend($log)
		console.info(message)
	}
}

/* let's create one (DOM) instance of this element,
   one logger to use accross the app (and keeps local logs history?) */
const $logger = document.createElement('log-info')
const log = $logger.log

export {
	$logger,
	log,
}
export default LogInfo
