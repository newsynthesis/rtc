/* a "real" modulo (only positive integer) */
const modulo = (number, mod) => {
	return ((number % mod) + mod) % mod
}

export default class GameMap extends HTMLElement {
	static get observedAttributes() {
		return ['data']
	}
	get data() {
		return JSON.parse(this.getAttribute('data'))
	}
	set data(obj) {
		this.setAttribute('data', JSON.stringify(obj))
	}
	constructor() {
		super()
		this.addEventListener('click', this.handleClick)
	}
	connectedCallback() {
		this.cursor = this.data[0]
		this.render()
	}
	attributeChangedCallback(attrName, oldVal, newVal) {
		this.render()
	}
	render = () => {
		this.innerHTML = `
			<svg version="1.1"
					 width="100%" height="100%"
					 xmlns="http://www.w3.org/2000/svg">
				<rect width="100%" height="100%" fill="orange" />
				${this.renderUsers()}
			</svg>
		`
		this.append()
	}
	renderUsers = () => {
		return (
			this.data
					.map(user => `
<foreignObject x="${user.x}" y="${user.y}" width="50" height="50">
<svg-user user="#!/bin/web" x="${user.x}" y="${user.y}"></svg-user>
</foreignObject>`)
					.reduce((all, item) => all + item)
		)
	}
	positionCursor(newPos = {
		x: 0,
		y: 0,
		z: 0,
	}) {
		const userPosition = new CustomEvent('userPosition', {
			bubbles: true,
			composed: true,
			detail: {newPos}
		})
		this.dispatchEvent(userPosition)
	}
	handleClick({layerX, layerY}) {
		this.positionCursor({
			x: layerX,
			y: layerY,
		})
	}
}
