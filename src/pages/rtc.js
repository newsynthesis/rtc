import {openpgp} from '../crypt/index.js'

export default class PageRTC extends HTMLElement {
	constructor() {
		super()
	}
	maxUser = 1
	async connectedCallback() {
		/* await this.generateKeyPair() */
		this.render()
	}
	async generateKeyPair() {
		const { privateKey, publicKey } = await openpgp.generateKey({
			type: 'rsa',
			rsaBits: 4096,
			userIDs: [{ name: 'focus-peer', email: 'peer@unonde.org' }],
			passphrase: 'focus-peer-unonde-pw'
		})
		this.privateKey = privateKey
		this.publicKey = publicKey
		const foundKeys = await openpgp.readKey({
			armoredKey: this.publicKey
		})
		this.userId = foundKeys.getFingerprint()
	}
	render = () => {
		this.innerHTML = ''
		for (let i = 0; i < this.maxUser; i++) {
			const $user = document.createElement('rtc-user')
			$user.setAttribute('manual-signaling', true)
			$user.setAttribute('uid', this.userId)
			this.rtcUser = $user
			this.append($user)
		}
	}
}
