import {$logger} from '../index.js'

/* a message */
const message = "LS0tLS1CRUdJTiBQR1AgTUVTU0FHRS0tLS0tCgp3eTRFQ1FNSWNDVFhnalc2d1liZ2doTHNRKzRSQVFFaWQxeWpxcUdDaWVQS3lwdFRhd2g2QWg3R0U2Z28KQ3R5RDByb0JzT0Q4dHNhZVFkaU05TEhReGl1eFdHQXUxRnlBcS94RzJIeFlMbUE4MFZ1TzdqN1B3Mit1CndqckIxLzZuNkJPMWpsSXZLRVlZTFV0TkIvSTJNZUc3Nm9VMVRhVzJVc1pLTTB4b2p0MTRTYW9wbG9qTQpDbU1OSzNLOGR3ZkI3UjdMWERpOHMzTnNOMFZtdEphMjA4OVhXaFhIbHNrRkQ0Vk9LYXhxUk9VQmZsbDYKQmJJc2loWTdROXFZdGtVejVIQUpwNEd2NGU3ZE5DZkgvVWlEYWY4VnlYUXRRTlUzWVIvT0tGaC9qTHE4Ck94NU9abUY4eTJ0Y01uQT0KPUVxYloKLS0tLS1FTkQgUEdQIE1FU1NBR0UtLS0tLQo="

const bgSrc = 'https://ugrp.gitlab.io/artworks/mx-frame-grid/?message=Zm9jdXM=&mono=true'

export default class PageHome extends HTMLElement {
	connectedCallback() {
		this.render()
	}
	render = () => {
		const $cryptInput = document.createElement('input-crypt')
		$cryptInput.setAttribute('message', message)

		const $bg = document.createElement('iframe')
		$bg.setAttribute('src', bgSrc)

		const $app = this
		$app.prepend($cryptInput)
		$app.prepend($logger)
		$app.prepend($bg)
	}
}
