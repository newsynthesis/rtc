// https://remotestoragejs.readthedocs.io/en/latest/data-modules/defining-a-module.html

// Remote Storage Module,
// defining a note document, a file.
// by default the file is plain text, but can be associated
// with an other file type from its file extension (ex: .css)
const UserModule = {
  name: 'rtcFocus',
  builder: function(privateClient, publicClient) {

		// privateClient.declareType('note', {
		// 	type: 'object',
		// 	properties: {
		// 		id: { type: 'string' },
		// 		content: { type: 'string' },
		// 		createdDate: { type: 'string' },
		// 		updatedDate: { type: 'string' }
		// 	},
		// 	required: ['content', 'id']
		// })

		return {
			exports: {
				privateClient: privateClient,
				async getAll() {
					const notes = await privateClient.getAll('', false)
					const ids = Object.keys(notes)
					const fileRequests = ids.map(id => privateClient.getFile(id))
					const files = await Promise.all(fileRequests)

					return files.map((file, index) => {
						file.id = ids[index]
						if(file.contentType === 'text/plain') {
							file.content = file.data
						}
						return file
					})
				},
				generateNoteId() {
					return Date.now().toString(36)
				},
				get(path) {
					// {data, contentType, revision}
					return privateClient.getFile(path)
				},
				delete(path) {
					return privateClient.remove(path)
				},
				storeNote({id, content, mimeType}) {
					console.log('storeNote')
					const note = {
						id,
						mimeType: mimeType || 'text/plain',
						content,
						updatedDate: Date.now().toString()
					}
					// return privateClient.storeObject('note', id, note)
					privateClient.storeFile(note.mimeType, note.id, note.content)
				}
			}
		}
  }
}

export default UserModule
